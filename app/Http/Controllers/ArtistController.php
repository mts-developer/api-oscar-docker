<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddNomineeArtistToOscarRequest;
use App\Http\Requests\CreateArtistRequest;
use App\Http\Requests\NomineeWinnerOrNoWinnerRequest;
use App\Http\Requests\RemoveNomineeArtistFromOscarRequest;
use App\Http\Requests\UpdateArtistRequest;
use App\Repositories\Contracts\ArtistRepositoryInterface;
use App\Responses\ErrorResponse;
use App\Responses\SuccessResponse;
use App\Traits\Controllers\Artist;
use Illuminate\Http\JsonResponse;

class ArtistController extends Controller
{
    private ArtistRepositoryInterface $repository;

    use Artist;

    public function __construct(ArtistRepositoryInterface $exception)
    {
        $this->repository = $exception;
    }

    public function store(CreateArtistRequest $request): JsonResponse
    {
        try {
            $data = $request->only(['name', 'stage_name', 'birth', 'birthplace', 'country', 'wikipedia']);
            $artist = $this->repository->store($data);

            return SuccessResponse::handle('Artist has been registered.', $artist->toArray());
        } catch (\Throwable $th) {
            return ErrorResponse::handle($th);
        }
    }

    public function update(UpdateArtistRequest $request, string $id): JsonResponse
    {
        try {
            $data = $request->only(['name', 'birth', 'birthplace', 'country', 'wikipedia']);
            $artist = $this->repository->update($id, $data);

            return SuccessResponse::handle('Artist has been updated.', $artist->toArray());
        } catch (\Throwable $th) {
            return ErrorResponse::handle($th);
        }
    }

    public function addNomineeArtistToOscar(AddNomineeArtistToOscarRequest $request, int $year): JsonResponse
    {
        $data = $request->only(['awardArtistId', 'artistId', 'movieId', 'winner']);
        $this->repository->addNomineeArtistToOscar($year, $data);

        return SuccessResponse::handle('Nominee has been registered to this award.');
    }

    public function removeNomineeArtistFromOscar(RemoveNomineeArtistFromOscarRequest $request, int $year): JsonResponse
    {
        $data = $request->only(['awardArtistId', 'artistId']);
        $this->repository->removeNomineeArtistFromOscar($year, $data);

        return SuccessResponse::handle('Nominee has been deleted from the ceremony');
    }

    public function nomineeWinnerOrNoWinner(NomineeWinnerOrNoWinnerRequest $request, int $year): JsonResponse
    {
        $data = $request->only(['awardArtistId', 'artistId', 'winner']);
        $this->repository->nomineeWinnerOrNoWinner($year, $data);

        return SuccessResponse::handle('Nominee has been updated to winner.');
    }
}
