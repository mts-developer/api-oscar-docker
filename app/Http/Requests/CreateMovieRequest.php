<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class CreateMovieRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'name' => 'required|unique:movie,name|string',
            'runtime' => 'required|integer',
            'release' => 'required|date',
            'language' => 'required|string|in:ZH,ES,EN,HI,ARB,PT,BN,RU,JA,PA,DE,JAV,TE,WU,MS,FR,VI,KO,TUR,IT,DE',
            'country' => 'required|string|in:CN,IN,US,ID,PK,BR,NG,BD,RU,JP,MX,PH,VN,ET,EG,DE,IR,TR,CD,FR,UK,SE',
            'company' => 'required|string',
            'wikipedia' => 'required|string|active_url',
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $response = response()->json([
            'timestamp' => now(),
            'status' => 500,
            'message' => 'Errors has been found.',
            'details' => $validator->errors(),
        ], 500);

        throw new HttpResponseException($response);
    }
}
