<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FindByNameArtistRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    protected function prepareForValidation(): void
    {
        $this->merge([
            "paginate" => $this->get('paginate') ?? false,
        ]);
    }

    public function rules(): array
    {
        return [
            "paginate" => "required|boolean",
            "per_pages" => "required_if:paginate,true|integer|min:1|max:100",
            "nominees" => "required|boolean",
            "only_winners_nominees" => "required_if:nominees,true|boolean",
        ];
    }
}
