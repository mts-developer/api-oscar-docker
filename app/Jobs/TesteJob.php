<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class TesteJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public function handle(): void
    {
        Log::info('====================================================');
        Log::info('TesteJob executado com sucesso!');
        Log::info('Teste Maneirão22222222222222222222');
        Log::info('Teste feito pra testar minha capacidade de aprender docker');
        Log::info('====================================================');
    }
}
