<?php

namespace App\Models;

use App\Traits\HasPrimaryKeyUuid;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Artist extends Model
{
    use HasPrimaryKeyUuid;

    protected $table = 'artist';

    protected $fillable = [
        'id',
        'name',
        'stage_name',
        'birth',
        'birthplace',
        'country',
        'wikipedia',
    ];

    protected $visible = [
        'id',
        'name',
        'stage_name',
        'birth',
        'birthplace',
        'country',
        'wikipedia',
        'nominees',
    ];

    public $timestamps = true;

    const NOMINEE_WINNER_MESSAGE = [
        'winner' => 'This artist is already winner in this award.',
        'noWinner' => 'This artist is not winner in this award.',
    ];

    public function nomineeArtistsRelation(): BelongsToMany
    {
        return $this->belongsToMany(AwardArtist::class, 'oscar_award_artist', 'oscarawardartist_id', 'artist_id')->withTimestamps();
    }

    public function nominees(): HasMany
    {
        return $this->hasMany(NomineeArtist::class);
    }
}
