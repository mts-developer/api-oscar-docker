<?php

namespace App\Observers;

use App\Helpers\ShortLinkApi;
use App\Models\Artist;
use GuzzleHttp\Exception\GuzzleException;
use JsonException;

class ArtistObserver
{
    /**
     * @throws GuzzleException
     * @throws JsonException
     */
    public function creating(Artist $artist): void
    {
        $artist->wikipedia = ShortLinkApi::getShortLink($artist->wikipedia);
    }

    /**
     * @throws GuzzleException
     * @throws JsonException
     */
    public function updating(Artist $artist): void
    {
        $artist->wikipedia = ShortLinkApi::getShortLink($artist->wikipedia);
    }
}
