<?php

namespace App\Repositories\Core\Eloquent;

use App\Exceptions\ArtistNotIsNomineeWithDuplicateMovieException;
use App\Exceptions\ExistsItAwardInCeremonyException;
use App\Exceptions\NomineeArtistAlreadyExistsException;
use App\Exceptions\NomineeIsAlreadyWinner;
use App\Exceptions\NomineeIsNotExistsException;
use App\Models\Artist;
use App\Models\OscarAwardArtist;
use App\Repositories\Contracts\ArtistRepositoryInterface;
use App\Repositories\Contracts\OscarRepositoryInterface;
use App\Repositories\Core\Eloquent\Verifications\NomineeArtistVerification;
use Illuminate\Support\Str;

class EloquentArtistRepository extends BaseEloquentRepository implements ArtistRepositoryInterface
{
    private OscarRepositoryInterface $oscar;

    use Traits\Artist;
    use NomineeArtistVerification;

    public function __construct(OscarRepositoryInterface $oscar)
    {
        parent::__construct();

        $this->oscar = $oscar;
    }

    public function entity(): string
    {
        return Artist::class;
    }

    /**
     * @throws ExistsItAwardInCeremonyException
     * @throws ArtistNotIsNomineeWithDuplicateMovieException
     * @throws NomineeArtistAlreadyExistsException
     */
    public function addNomineeArtistToOscar(string $yearOscar, array $data): void
    {
        $oscarAward = $this->getOscarAwardArtist($yearOscar, $data['awardArtistId']);

        $this->verifyIfExistsItAwardInCeremony($oscarAward);
        $this->verifyIfArtistIsAlreadyNominee($oscarAward, $data);
        $this->verifyIfArtistNotIsNomineeWithDuplicateMovie($oscarAward, $data);

        $oscarAward->nomineeArtistsRelation()->attach($data['artistId'],
            ['id' => Str::uuid(), 'movie_id' => $data['movieId'], 'winner' => $data['winner'], 'created_at' => now(), 'updated_at' => now()], false);
    }

    /**
     * @throws ExistsItAwardInCeremonyException
     * @throws NomineeIsNotExistsException
     */
    public function removeNomineeArtistFromOscar(string $yearOscar, array $data): void
    {
        $oscarAward = $this->getOscarAwardArtist($yearOscar, $data['awardArtistId']);
        $this->verifyIfExistsItAwardInCeremony($oscarAward);
        $this->verifyIfExistsNominee($oscarAward, $data);

        $oscarAward->nomineeArtistsRelation()->detach($data['artistId']);
    }

    /**
     * @throws ExistsItAwardInCeremonyException
     * @throws NomineeIsAlreadyWinner
     * @throws NomineeIsNotExistsException
     */
    public function nomineeWinnerOrNoWinner(string $yearOscar, array $data): void
    {
        $oscarAward = $this->getOscarAwardArtist($yearOscar, $data['awardArtistId']);
        $winner = $data['winner'] ? 'winner' : 'noWinner';

        $this->verifyIfExistsItAwardInCeremony($oscarAward);
        $this->verifyIfExistsNominee($oscarAward, $data);

        $nominee = $oscarAward->nomineeArtists()->where('artist_id', $data['artistId'])->first();
        $nominee->winner = $data['winner'];

        if (! $nominee->isDirty()) {
            throw new NomineeIsAlreadyWinner(Artist::NOMINEE_WINNER_MESSAGE[$winner]);
        }
        $nominee->save();
    }

    private function getOscarAwardArtist(string $yearOscar, $awardArtistId): OscarAwardArtist
    {
        $oscarId = $this->oscar->findOscarByYear($yearOscar)->id;

        return OscarAwardArtist::where('awardartist_id', $awardArtistId)->where('oscar_id', $oscarId)->first();
    }
}
