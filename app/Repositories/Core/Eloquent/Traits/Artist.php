<?php

namespace App\Repositories\Core\Eloquent\Traits;

use App\Models\Artist as ArtistModel;
use Illuminate\Support\Collection;

trait Artist
{
    public function findByName(string $name, array $rules)
    {
        $rules = (object)$rules;

        $query = $this->entity->where(static function ($query) use ($name) {
            $query->where('stage_name', 'like', '%' . $name . '%')
                ->orWhere('name', 'like', '%' . $name . '%');
        });

        $query = $rules->nominees ? $query->with([
            'nominees' => function ($query) {
                $query->where('winner', true)->with([
                    'movie',
                    'edition.oscar',
                    'edition.award',
                ]);
            },
        ]): $query;

        return $rules->paginate ? $query->paginate($rules->per_pages) : $query->get();
    }

    public function findById(string $id):ArtistModel
    {
        return $this->entity->with([
            'nominees.movie',
            'nominees.edition.oscar',
            'nominees.edition.award',
        ])->findOrFail($id);
    }
}
