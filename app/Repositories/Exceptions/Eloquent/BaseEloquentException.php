<?php

namespace App\Repositories\Exceptions\Eloquent;

use App\Exceptions\NotRepositoryExceptionDefined;
use App\Repositories\Contracts\ExceptionRepositoryInterface;
use App\Repositories\Contracts\RepositoryInterface;

abstract class BaseEloquentException implements ExceptionRepositoryInterface
{
    protected int $timeCache = 60 * 60;

    protected RepositoryInterface $repository;

    /**
     * @throws NotRepositoryExceptionDefined
     */
    public function __construct()
    {
        $this->repository = $this->resolveRepository();
    }

    /**
     * @throws NotRepositoryExceptionDefined
     */
    public function resolveRepository()
    {
        if (! method_exists($this, 'repository')) {
            throw new NotRepositoryExceptionDefined();
        }

        return app($this->repository());
    }
}
