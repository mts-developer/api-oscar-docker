<?php

namespace App\Responses;

use Illuminate\Http\JsonResponse;

class CustomErrorResponse
{
    public static function handle(\Throwable $e, string $message, array $details = []): JsonResponse
    {
        return response()->json([
            'timestamp' => now(),
            'status' => 500,
            'message' => $message ?? $e->getMessage(),
            'details' => $details,
        ], 500);
    }
}
