<?php

namespace App\Responses;

use Illuminate\Http\JsonResponse;

class SuccessResponse
{
    public static function handle(string $message, array $response = null): JsonResponse
    {
        return response()->json([
            'timestamp' => now(),
            'status' => 200,
            'message' => $message,
            'data' => [
                $response ?? null,
            ],
        ], 200);
    }
}
