<?php

namespace App\Traits\Controllers;

use App\Http\Requests\FindByIdArtistRequest;
use App\Http\Requests\FindByNameArtistRequest;
use App\Responses\CustomErrorResponse;
use App\Responses\ErrorResponse;
use App\Responses\SuccessResponse;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;

trait Artist
{
    public function findById(FindByIdArtistRequest $request): JsonResponse
    {
        $artist = $this->repository->findById($request->id);

        return SuccessResponse::handle('Artist has been found.', $artist->toArray());
    }

    public function findByName(FindByNameArtistRequest $request, string $name): JsonResponse
    {
        try {
            $data = $request->only(['paginate', 'per_pages', 'nominees', 'only_winners_nominees']);
            $artist = $this->repository->findByName($name, $data);

            return SuccessResponse::handle('Artist has been found.', $artist->toArray());
        } catch (ModelNotFoundException $e) {
            return CustomErrorResponse::handle(e:$e, message:'Artist hasn\'t not found.', details: [
                'name' => 'Artist name not found.',
                'stage_name' => 'Artist stage name not found.',
            ]);
        } catch (\Throwable $th) {
            return ErrorResponse::handle($th);
        }
    }
}
