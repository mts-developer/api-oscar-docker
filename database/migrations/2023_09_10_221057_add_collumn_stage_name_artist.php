<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up(): void
    {
        Schema::table('artist', static function (Blueprint $table) {
            $table->addColumn('string', 'stage_name', ['length' => 255, 'after' => 'name']);
        });
    }

    public function down(): void
    {
        Schema::table('artist', static function (Blueprint $table) {
            $table->dropColumn('stage_name');
        });
    }
};
