<?php

/** @noinspection ALL */

use App\Http\Controllers\ArtistController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\AwardArtistController;
use App\Http\Controllers\AwardMovieController;
use App\Http\Controllers\MovieController;
use App\Http\Controllers\OscarController;
use Illuminate\Support\Facades\Route;


Route::group(['middleware' => 'api', 'prefix' => 'auth'], function ($router) {
    Route::post('/login', [AuthController::class, 'login']);
});

//Route::group(['middleware' => ['jwt.auth']], function () {
    Route::post('/oscar', [OscarController::class, 'store']);
    Route::put('/oscar/{year}', [OscarController::class, 'update']);
    Route::delete('/oscar/{year}', [OscarController::class, 'delete']);

    Route::post('/award/artist', [AwardArtistController::class, 'store']);
    Route::post('/award/artist/oscar/{year}/{awardArtistId}', [AwardArtistController::class, 'addAwardToOscar']);
    Route::delete('/award/artist/oscar/{year}/{awardArtistId}', [AwardArtistController::class, 'removeAwardFromOscar']);

    Route::post('/award/movie', [AwardMovieController::class, 'store']);
    Route::post('/award/movie/oscar/{year}/{awardMovieId}', [AwardMovieController::class, 'addAwardToOscar']);
    Route::delete('/award/movie/oscar/{year}/{awardMovieId}', [AwardMovieController::class, 'removeAwardFromOscar']);

    Route::post('/artist', [ArtistController::class, 'store']);
    Route::put('/artist/{id}', [ArtistController::class, 'update']);

    Route::post('/artist/oscar/nominee/{year}', [ArtistController::class, 'addNomineeArtistToOscar']);
    Route::delete('/artist/oscar/nominee/{year}', [ArtistController::class, 'removeNomineeArtistFromOscar']);
    Route::patch('/artist/oscar/winner/{year}', [ArtistController::class, 'nomineeWinnerOrNoWinner']);

    Route::post('/movie', [MovieController::class, 'store']);
    Route::put('/movie/{id}', [MovieController::class, 'update']);

    Route::post('/movie/oscar/nominee/{year}', [MovieController::class, 'addNomineeMovieToOscar']);
    Route::delete('/movie/oscar/nominee/{year}', [MovieController::class, 'removeNomineeMovieFromOscar']);
    Route::patch('/movie/oscar/winner/{year}', [MovieController::class, 'nomineeWinnerOrNoWinner']);
//});


/* Routes Oscar Ceremony */
Route::get('/oscar', [OscarController::class, 'findAll']);
Route::get('/oscar/{year}', [OscarController::class, 'findOscarByYear']);


/* Awards Artists */
Route::get('/award/artist/{id}', [AwardArtistController::class, 'findById']);

/* Awards Movies */
Route::get('/award/movie/{id}', [AwardMovieController::class, 'findById']);

/* Artist */
Route::get('/artist/{id}', [ArtistController::class, 'findById']);
Route::get('/artist/name/{name}', [ArtistController::class, 'findByName']); // Retorna os dados do artisa apenas com os prêmios que ele ganhou

/* Movie */
Route::get('/movie/{id}', [MovieController::class, 'findById']);
